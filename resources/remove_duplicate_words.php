<?php
#Remove duplicate words from a text file with 1 word per line.
$words = file('wordlist.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$words = array_unique($words, SORT_STRING);
natsort($words);
$words = array_values($words);
$words = implode("\n", $words);
file_put_contents('wordlist_unique.txt', $words);
?>