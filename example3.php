<?php
$location = realpath(dirname(__FILE__));
require_once $location . '/ritchey_increment_words_i1_v2.php';
$string = '';
$time_stamp = date_timestamp_get(date_create());
$alarm = $time_stamp + 60;
while (date_timestamp_get(date_create()) < $alarm){
	$string = ritchey_increment_words_i1_v2($string, TRUE);
}
if ($string === FALSE){
	echo "FALSE\n";
} else {
	print_r($string);
	echo "\n";
}
?>