<?php
#Name:Ritchey Increment Words i1 v2
#Description:Increment words by 1 valid increment using content generation techniques. Returns text as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Unsupported words will trigger failure.
#Arguments:'string' is a string containing the text to increment. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_increment_words_i1_v2') === FALSE){
function ritchey_increment_words_i1_v2($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	} else if (preg_replace('/([a-z]| |\!|\.|\?|[A-Z])/', '', $string) != ''){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Add a space before any punctuation
		$string = @preg_replace('/\!/', ' !', $string);
		$string = @preg_replace('/\./', ' .', $string);
		$string = @preg_replace('/\?/', ' ?', $string);
		###Uncapitalize words
		$string = @strtolower($string);
		###Break into an array of words. Reverse the array so that increment can be done from end to start of string.
		$string = @explode(' ', $string);
		$string = @array_filter($string);
		$string = @array_reverse($string);
		###Increment the first word between '' and "zebra". When it exceeds "zebra" it loops back to "!", and the next value is incremented by 1. If a next doesn't exist create it as "!".
		$location = realpath(dirname(__FILE__));
		$source = "{$location}/assets/wordlist.txt";
		$wordlist = file($source, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		foreach ($string as &$value) {
			if ($value === ''){
				$value = '!';
				goto break_increment;
			} else if ($value == TRUE) {
				$check = FALSE;
				for ($i = 0; $i < count($wordlist); $i++) {
					$ii = $i + 1;
					if ($value === $wordlist[$i]) {
						if ($ii < count($wordlist)){
							$value = $wordlist[$ii];
							goto break_increment;
						} else {
							$value = '!';
						}
						$check = TRUE;
					}
				}
				if ($check === FALSE){
					$errors[] = "wordlist (unsupported string:\"{$value}\")";
					goto result;
				}
			}
		}
		unset($value);
		break_increment:
		###Unreverse array
		$string = @array_reverse($string);
		###Check if entire array is '!'s, and if it is append '!' to the end to increment to next range.
		$check = FALSE;
		foreach ($string as &$value) {
			if ($value !== '!'){
				$check = TRUE;
			}
		}
		unset($value);
		if ($check === FALSE){
			$string[] = '!';
		}
		###Convert array to string
		$string = @implode(' ', $string);
		###Remove space before any punctuation
		$string = @preg_replace('/ \!/', '!', $string);
		$string = @preg_replace('/ \./', '.', $string);
		$string = @preg_replace('/ \?/', '?', $string);
		###Remove white space at beginning of string.
		$string = @trim($string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_increment_words_i1_v2_format_error') === FALSE){
				function ritchey_increment_words_i1_v2_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_increment_words_i1_v2_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>